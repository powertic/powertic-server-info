<?php

header("Content-type:application/json");

require_once __DIR__ . '/vendor/autoload.php';

$linfo = new \Linfo\Linfo();

$parser = $linfo->getParser();

$array = array();
$out[] = $parser->getOS();
$out[] = $parser->getRam();
$out[] = $parser->getCPU();
$out[] = $parser->getLoad();
$out[] = $parser->getCPUUsage();

echo json_encode( $out );
